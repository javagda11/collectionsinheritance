package com.sdaa.polimorfizmZadanie.figury;

import com.sdaa.polimorfizmZadanie.Figura;

public class Trapez extends Figura {
    private int bokA;
    private int bokB;
    private int h;

    public Trapez(int a, int b, int h) {
        this.bokA = a;
        this.bokB = b;
        this.h = h;
    }

    @Override
    public int getPowierzchnia(){
        return (int) (((bokA + bokB) * h) / 2.0);
    }
}
