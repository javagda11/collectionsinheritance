package com.sdaa.polimorfizmZadanie.figury;

import com.sdaa.polimorfizmZadanie.Figura;

public class Kwadrat extends Figura {
    private double bokA;

    public Kwadrat(double bokA) {
        this.bokA = bokA;
    }

    @Override
    public int getPowierzchnia() {
        return (int) (bokA * bokA);
    }
}
