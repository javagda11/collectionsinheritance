package com.sdaa.polimorfizmZadanie.figury;

import com.sdaa.polimorfizmZadanie.Figura;

public class Kolo extends Figura {
    private double promien;

    public Kolo(int promien) {
        this.promien = promien;
    }

    public int getPowierzchnia(){
        return (int) (Math.PI * promien * promien);
    }
}
