package com.sda.familyMembersZadanie;

public class Main {
    public static void main(String[] args) {
        FamilyMember[] członkowieRodziny = new FamilyMember[5];

        członkowieRodziny[0] = new Mother();
        członkowieRodziny[1] = new Father();
        członkowieRodziny[2] = new Son();
        członkowieRodziny[3] = new Daughter();
        członkowieRodziny[4] = new Son();

        for (FamilyMember czlonekRodziny : członkowieRodziny) {
            czlonekRodziny.przedstawSie();

            if (czlonekRodziny instanceof Father) {
                Father rzutowany = (Father) czlonekRodziny;
                rzutowany.piwko();
            }
        }
    }
}
