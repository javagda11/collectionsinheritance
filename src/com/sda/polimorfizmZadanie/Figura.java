package com.sda.polimorfizmZadanie;

public class Figura {

    private int powierzchnia;

    public Figura(int powierzchnia) {
        this.powierzchnia = powierzchnia;
    }

    public int getPowierzchnia() {
        return powierzchnia;
    }
}
