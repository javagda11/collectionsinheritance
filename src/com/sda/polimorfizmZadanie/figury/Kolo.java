package com.sda.polimorfizmZadanie.figury;

import com.sda.polimorfizmZadanie.Figura;

public class Kolo extends Figura {
    private double promien;

    public Kolo(int promien) {
        super((int) (Math.PI * promien * promien));
        this.promien = promien;
    }
}
