package com.sda.polimorfizmZadanie.figury;

import com.sda.polimorfizmZadanie.Figura;

public class Kwadrat extends Figura {
    private double bokA;
    private double bokB;

    public Kwadrat(double bokA, double bokB) {
        super((int) (bokA * bokB));
        this.bokA = bokA;
        this.bokB = bokB;
    }
}
