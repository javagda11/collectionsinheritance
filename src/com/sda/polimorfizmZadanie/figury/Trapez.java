package com.sda.polimorfizmZadanie.figury;

import com.sda.polimorfizmZadanie.Figura;

public class Trapez extends Figura {
    private int bokA;
    private int bokB;
    private int h;

    public Trapez(int a, int b, int h) {
        super((int) (((a + b) * h) / 2.0));

        this.bokA = a;
        this.bokB = b;
        this.h = h;
    }
}
