package com.sda.polimorfizmZadanie;

import com.sda.polimorfizmZadanie.figury.Kolo;
import com.sda.polimorfizmZadanie.figury.Kwadrat;
import com.sda.polimorfizmZadanie.figury.Trapez;

public class Main {
    public static void main(String[] args) {
        Figura[] tablicaFigur = new Figura[5];

        tablicaFigur[0] = new Kwadrat(10, 5);
        tablicaFigur[1] = new Kolo(20);
        tablicaFigur[2] = new Trapez(5, 7, 3);
        tablicaFigur[3] = new Kwadrat(40, 40);
        tablicaFigur[4] = new Kolo(50);

        int iloscKubelkow = SymulatorFarby.obliczZapotrzebowanieNaFarbe(tablicaFigur, 50);

        System.out.println("Potrzeba: " + iloscKubelkow);

//        Math.PI
    }
}
