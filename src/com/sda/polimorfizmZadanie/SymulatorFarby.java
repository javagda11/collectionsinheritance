package com.sda.polimorfizmZadanie;

public class SymulatorFarby {
    public static int obliczZapotrzebowanieNaFarbe(Figura[] figury, double wielkoscPojemnika) {
        int sumaPowierzchniFigur = 0;

        for (Figura figura : figury) {
            sumaPowierzchniFigur += figura.getPowierzchnia();
        }

        double iloscWiader = sumaPowierzchniFigur / wielkoscPojemnika;

        // 1.1 -> int -> 1
        // 1.999999999999999 -> int -> 1
        //
        return (int) Math.ceil(iloscWiader);
    }
}
