package com.sda.exceptions.przyklad1;

public class Obywatel {
    private String imieNazwisko;
    private String numerTel;
    private int rokUrodzenia, mscUrodzenia, dzienUrodzenia;
    private Plec plec;

    public Obywatel(String imieNazwisko, String numerTel, int rokUrodzenia, int mscUrodzenia, int dzienUrodzenia, Plec plec) {
        this.imieNazwisko = imieNazwisko;
        this.numerTel = numerTel;
        this.rokUrodzenia = rokUrodzenia;
        this.mscUrodzenia = mscUrodzenia;
        this.dzienUrodzenia = dzienUrodzenia;
        this.plec = plec;
    }

    @Override
    public String toString() {
        return "Obywatel{" +
                "imieNazwisko='" + imieNazwisko + '\'' +
                ", numerTel='" + numerTel + '\'' +
                ", rokUrodzenia=" + rokUrodzenia +
                ", mscUrodzenia=" + mscUrodzenia +
                ", dzienUrodzenia=" + dzienUrodzenia +
                ", plec=" + plec +
                '}';
    }
}
