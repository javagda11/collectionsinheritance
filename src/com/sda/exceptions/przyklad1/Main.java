package com.sda.exceptions.przyklad1;

import com.sda.familyMembersZadanie.FamilyMember;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Podaj imie i nazwisko: ");
        String imieInazwisko = scanner.nextLine();

        ///////////////////////////////////////////////////////////
        //Przechodzę do sprawdzania numeru telefonu
        ///////////////////////////////////////////////////////////

        String numerTel = null;
        do {
            System.out.println("Podaj numer telefonu: ");
            // pobieram numer tel
            numerTel = scanner.nextLine();

            if (numerTel.length() != 9) {
                System.out.println("Numer telefonu powinien mieć 9 znaków");
                // weryfikuje długość numeru tel. jeśli
                // zeby wymusić powtórzenie ustawiam numerTel = null, wtedy wykona się kolejne sprawdzenie while i dojdzie do powtórzenia pętli
                numerTel = null;
                continue;
            }

            for (int i = 0; i < numerTel.length(); i++) {
                // znak na pozycji i jest większy lub równy 48 oraz mniejszy równy 57
//                if (numerTel.charAt(i) >= 48 && numerTel.charAt(i) <= 57) {
    //                if (numerTel.charAt(i) < '0' || numerTel.charAt(i) > '9') {
                if (!(numerTel.charAt(i) >= '0' && numerTel.charAt(i) <= '9')) {
                    numerTel = null;
                    System.out.println("Niepoprawny numer telefonu, używaj samych (9) cyfr.");
                    // weryfikuje że znaki to cyfry
                    break;
                }
            }

        } while (numerTel == null);
        System.out.println("Zatwierdzony (poprawny) numer telefonu!");

        ///////////////////////////////////////////////////////////
        //Przechodzę do sprawdzania roku urodzenia
        ///////////////////////////////////////////////////////////

        Integer rokUr = null;
        do {
            try {
                System.out.println("Podaj rok urodzenia: ");
                String rokUrodzenia = scanner.nextLine();

                rokUr = Integer.parseInt(rokUrodzenia);
                if (rokUr < 1920 || rokUr > 2018) {
                    // zeby wymusić powtórzenie ustawiam rokUr = null, wtedy wykona się kolejne sprawdzenie while i dojdzie do powtórzenia pętli
                    rokUr = null;
                }
            } catch (NumberFormatException nfe) {
                System.out.println("Niepoprawny format liczby.");
                System.out.println("Spróbuj ponownie!");
            }
        } while (rokUr == null);
        System.out.println("Zatwierdzono rok urodzenia.");

        ///////////////////////////////////////////////////////////
        //Przechodzę do sprawdzania miesiąca urodzenia
        ///////////////////////////////////////////////////////////

        Integer miesiacUr = null;
        do {
            try {
                System.out.println("Podaj miesiac urodzenia: ");
                String miesiac = scanner.nextLine();

                miesiacUr = Integer.parseInt(miesiac);
                if (miesiacUr < 1 || rokUr > 12) {
                    // zeby wymusić powtórzenie ustawiam miesiacUr = null, wtedy wykona się kolejne sprawdzenie while i dojdzie do powtórzenia pętli
                    miesiacUr = null;
                }
            } catch (NumberFormatException nfe) {
                System.out.println("Niepoprawny format liczby.");
                System.out.println("Spróbuj ponownie!");
            }
        } while (miesiacUr == null);
        System.out.println("Zatwierdzono miesiąc urodzenia.");

        ///////////////////////////////////////////////////////////
        //Przechodzę do sprawdzania dnia urodzenia
        ///////////////////////////////////////////////////////////

        Integer dzienUr = null;
        do {
            try {
                System.out.println("Podaj dzien urodzenia: ");
                String dzien = scanner.nextLine();

                dzienUr = Integer.parseInt(dzien);
                if (dzienUr < 1 || dzienUr > 30) {
                    dzienUr = null;
                }
            } catch (NumberFormatException nfe) {
                System.out.println("Niepoprawny format liczby.");
                System.out.println("Spróbuj ponownie!");
            }
        } while (dzienUr == null);
        System.out.println("Zatwierdzono dzień urodzenia.");

        ///////////////////////////////////////////////////////////
        //Przechodzę do sprawdzania płci
        ///////////////////////////////////////////////////////////

        Plec plec = null;
        do {
            try {
                System.out.println("Podaj swoją płeć (KOBIETA/MEZCZYZNA): ");
                String plecString = scanner.nextLine();

                plec = Plec.valueOf(plecString.toUpperCase());
            } catch (IllegalArgumentException iae) {
                System.out.println("Niepoprawna opcja, wpisz kobieta/mezczyzna! Spróbuj ponownie!");
            }
        } while (plec == null);
        System.out.println("Zatwierdzono płeć!");

        ///////////////////////////////////////////////////////////
        //Z podanych danych konstruuje obiekt
        ///////////////////////////////////////////////////////////

        Obywatel obywatel = new Obywatel(imieInazwisko, numerTel, rokUr, miesiacUr, dzienUr, plec);
        System.out.println(obywatel);

    }
}
